﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_1._7
{
    /// <summary>
    /// Maps client class to bd table
    /// </summary>
    public class ClientMapping : IEntityTypeConfiguration<Client>
    {
        private const string TableName = "Clients";
        private const int MaxSize = 1000;
        public void Configure(EntityTypeBuilder<Client> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.FirstName);
            builder.Property(x => x.LastName);
            builder.Property(x => x.BankAccountNumber);
            builder.Property(x => x.CreditCardNumber);
            builder.Property(x => x.Patronimic);
            builder.Property(x => x.Address);
        }
    }

}
