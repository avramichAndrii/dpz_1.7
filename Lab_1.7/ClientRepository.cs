﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lab_1._7
{
    /// <summary>
    /// Operates with db data
    /// </summary>
    public class ClientRepository
    {
        private readonly LabContext _labContext;
        const string UpdateMessage = "Couldn't be updated. Please check sending data.";
        const string DeleteMessage = "Couldn't be deleted.";
        const string CreateMessage = "Couldn't be created. Please check sending data.";
        public ClientRepository()
        {
            _labContext = new LabContext();
        }

        /// <summary>
        /// Creates new client
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public async Task<Client> CreateAsync(Client client)
        {
            try
            {
                _labContext.ClientModels.Add(client);
                await _labContext.SaveChangesAsync();
                return client;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataMenuDomainException(CreateMessage);
            }
        }

        /// <summary>
        /// Deletes client fron db
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task DeleteAsync(Guid clientId)
        {
            Client modelToDelete = await _labContext.ClientModels.SingleAsync(model => model.Id == clientId);
            if (modelToDelete == null)
            {
                throw new InvalidDataMenuDomainException(DeleteMessage);
            }
            try
            {
                _labContext.ClientModels.Remove(modelToDelete);
            }
            catch (InvalidOperationException)
            {
                throw new InvalidDataMenuDomainException(DeleteMessage);
            }
        }

        /// <summary>
        /// Gets client by id
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<Client> GetByIdAsync(Guid clientId)
        {
            var res = await _labContext.ClientModels.SingleAsync(dishModel => dishModel.Id == clientId);
            if (res == null)
            {
                throw new InvalidDataMenuDomainException(CreateMessage);
            }
            return res;
        }
    }
}
