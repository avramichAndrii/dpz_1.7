﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_1._7
{
    /// <summary>
    /// Data context of our service
    /// </summary>
    /// //### This is level 3 header for doxygen######
    public sealed class LabContext : DbContext
    {
        /// <summary>
        /// Db context
        /// </summary>
        public LabContext() : base()
        {

        }
        /// <summary>
        /// Db client rows represented as list
        /// </summary>
        public DbSet<Client> ClientModels { get; set; }

        /// <summary>
        /// Fluent API mapping
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration<Client>(new ClientMapping());
            base.OnModelCreating(modelBuilder);
        }
    }

}
