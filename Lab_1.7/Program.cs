﻿using System;

namespace Lab_1._7
{
    /// <summary>
    /// Program startup file
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            ClientRepository repository = new ClientRepository();
            Guid client = Guid.NewGuid();
            Console.WriteLine(repository.GetByIdAsync(client).Result.Address);
        }
    }
}
