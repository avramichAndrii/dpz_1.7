﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;

namespace Lab_1._7
{
    /// <summary>
    /// Class represents service Client
    /// </summary>
    public class Client
    {
        /// <summary>
        ///Class identificator
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Clients Last name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Client first name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Client Patronimic
        /// </summary>
        public string Patronimic { get; set; }
        /// <summary>
        /// Client address
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// Client credit card number
        /// </summary>
        public SecureString CreditCardNumber { get; set; }

        public SecureString BankAccountNumber { get; set; }
    }
}
