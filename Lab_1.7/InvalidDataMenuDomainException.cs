﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Lab_1._7
{
    public class InvalidDataMenuDomainException : Exception
    {
        public InvalidDataMenuDomainException() : base() { }

        public InvalidDataMenuDomainException(string message) : base(message) { }

        public InvalidDataMenuDomainException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args)) { }
    }

}
